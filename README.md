# Installation

```Bash
pip install -r requirements.txt --user
```

# Usage

```Python
python __main__.py
```

# Known issues

* Insecure. KeepassHTTP keys are store unencrypted on the disk.
* Supports Ed25519 keys only
