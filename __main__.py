import io
import logging
import re
import socket
import time
from getpass import getpass
from subprocess import PIPE, Popen
# import threading

import keepasshttp
from paramiko.ed25519key import Ed25519Key
from paramiko.agent import AgentSSH
from paramiko.ssh_exception import PasswordRequiredException

logger = logging.getLogger(__name__)


class UnixDomainSocketAgent(AgentSSH):
    def __init__(self, sock_path):
        conn = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        conn.connect(sock_path)
        self._connect(conn)


class SSHAgentProcess:
    def __init__(self):
        self.ssh_agent_proc = Popen(['ssh-agent', '-D'], stdout=PIPE)
        first_line = self.ssh_agent_proc.stdout.readline().decode('ascii')
        logger.debug('first line from ssh-agent:\n' + first_line)
        mobj = re.search(r'SSH_AUTH_SOCK=([^;]+);', first_line)
        if not mobj:
            raise RuntimeError('Can\'t start ssh-agent')
        self.sock_path = mobj.group(1)

    def terminate(self):
        self.ssh_agent_proc.terminate()


def add_keys_from_logins(entries, agent):
    for entry in entries:
        for field in entry['StringFields']:
            if field['Key'] == 'KPH: ed25519_key':
                priv_key = field['Value']
                priv_key_file = io.StringIO(priv_key)
                try:
                    pkey = Ed25519Key.from_private_key(priv_key_file)
                except PasswordRequiredException:
                    priv_key_file.seek(0, io.SEEK_SET)
                    pkey = Ed25519Key.from_private_key(
                        priv_key_file,
                        # encode is necessary
                        # see https://github.com/paramiko/paramiko/pull/1051
                        password=getpass('Key passrase:').encode('ascii'))
                logger.debug(
                    'Adding an Ed25519 key: ' + pkey.comment.decode('ascii'))
                agent.add_key(pkey)


def main():
    logging.basicConfig()
    logger.setLevel(logging.DEBUG)
    session = keepasshttp.start('python-keepassxc-agent')
    entries = session.get_logins('https://ssh-key')

    ssh_agent_proc = SSHAgentProcess()
    try:
        logger.info('export SSH_AUTH_SOCK=%s' % ssh_agent_proc.sock_path)
        agent = UnixDomainSocketAgent(ssh_agent_proc.sock_path)
        add_keys_from_logins(entries, agent)
        while True:
            time.sleep(1)
    finally:
        ssh_agent_proc.terminate()


if __name__ == '__main__':
    main()
